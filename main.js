'use strict'

const {electron, ipcMain, app, BrowserWindow} = require('electron')
const path = require('path')
const url = require('url')
const vibrancy = require('electron-vibrancy')
const request = require('request')
const fs = require('fs')
const Zip = require('adm-zip')
const glob = require('glob')

//require('electron-reload')(__dirname)

let win

const dev = false
const cacheDir = path.resolve(__dirname, ( dev ? 'cache' :  '../../cache'))

function createWindow () {
  win = new BrowserWindow({
    width: 985, 
    height: 585, 
    minWidth: 985, 
    minHeight: 185,
    resizable: true, 
    frame: false, 
    transparent: true,
    vibrancy: 'ultra-dark',
    icon: path.join(__dirname, 'assets/appicons/ico/icon.ico')
  })

  vibrancy.SetVibrancy(win, 0)

  win.loadURL(url.format({
    pathname: path.join(__dirname, 'index.html'),
    protocol: 'file:',
    slashes: true
  }))

  win.on('closed', function() {
    process.exit();
  });

  ipcMain.on('download', (event, info) => {
    win.webContents.send('dls')
    let req = request.get({
      url: 'https://beatsaver.com/download/' + info.key,
      encoding: 'binary'
    }, (err, r, data) => {
      if(r.statusCode == 404) {
        win.webContents.send('dlf', {cause: 'Error 404', ring: info.ring, p: info.p, per: 100})
        return
      }
      fs.writeFile(info.path + '\\' + info.key + '.zip', data, 'binary', (err) => {
        if(err) {
          win.webContents.send('dlf')
          return
        }
        let z = new Zip(info.path + '\\' + info.key + '.zip')
        let p = new Promise(function(resolve, reject) {
          z.extractAllTo(info.path + '\\' + info.key)
          resolve()
        })
        p.then(() => {
          fs.unlink(info.path + '\\' + info.key + '.zip', () => {
            if(Object.keys(info['q']).length) {
              win.webContents.send('dlcq', info.q)
            } else {
              win.webContents.send('dlc', {s: info.s, p: info.p})
            }
          })
        })
      })
    })

    let len = 0
    let chunks = 0

    let ring = info.ring

    req.on('data', (chunk) => {
      chunks += chunk.length
      win.webContents.send('prog', {per: Math.trunc((chunks / len) * 100), ring: ring})
    })

    req.on('response', (data) => {
      len = data.headers['content-length']
    })
  })

  ipcMain.on('preview', (event, info) => {
    if(info.songsDir) {
      fs.readdir(info.songsDir, (err, names) => {
        fs.readdir(path.join(info.songsDir, names[names.indexOf(info.key)]), (err, songName) => {
          fs.readdir(path.join(info.songsDir, names[names.indexOf(info.key)], songName[0]), (err, files) => {
            for(let i = 0; i < files.length; i++) {
              if(files[i].includes('.ogg') || files[i].includes('.mp3') || files[i].includes('.wav')) {
                win.webContents.send('prevpath', {file: path.join(info.songsDir, names[names.indexOf(info.key)], songName[0], files[i]), s: info.s})
                return
              }
            }
          })
        })
      })
    } else {
      let req = request.get({
        url: 'https://beatsaver.com/download/' + info.key,
        encoding: 'binary'
      }, (err, r, data) => {
        if(r.statusCode == 404) {
          win.webContents.send('prevf', 'Error 404. BeatSaver done messed up!')
          return
        }
        fs.writeFile('./cache\\preview\\preview.zip', data, 'binary', (err) => {
          if(err) {
            return
          }
          let z = new Zip(cacheDir + '/preview/preview.zip')
          let p = new Promise(function(resolve, reject) {
            z.extractAllTo(cacheDir + '/preview')
            resolve()
          })
          p.then(() => {
            fs.unlink('./cache/preview/preview.zip', () => {
              fs.readdir(cacheDir + '/preview', (err, names) => {
                fs.readdir(cacheDir + '/preview/' + names[0], (err, files) => {
                  for(let i = 0; i < files.length; i++) {
                    if(files[i].includes('.ogg') || files[i].includes('.mp3') || files[i].includes('.wav')) {
                      win.webContents.send('prevpath', {file: cacheDir + '\\preview\\' + names[0] + '\\' + files[i], s: info.s})
                      return
                    }
                  }
                })
              })
            })
          })
        })
      })
    }
  })
}

app.on('ready', createWindow)